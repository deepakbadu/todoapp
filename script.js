const element = document.getElementById('btn')
const ul = document.getElementById('todolist')
element.addEventListener("click", function(){
  let arr = localStorage.getItem("todoItem");
  arr = arr?JSON.parse(arr):[]
  const text = document.getElementById('input').value
  if(text === ''){
    alert("Please enter your task")
  }else {
    arr.push(text)
    localStorage.setItem("todoItem",JSON.stringify(arr))
    document.getElementById('input').value = ''
    getLocalValue()
  }
})

function getLocalValue() {
  let ul = document.getElementById('todolist')
  ul.innerHTML = '';
  let retrivedObject = localStorage.getItem("todoItem")
  let stored = JSON.parse(retrivedObject)
  for(let i=0; i<stored.length;i++){
    let li = document.createElement('li')
    let btn = document.createElement('button')
    let edit = document.createElement('button')
    edit.innerHTML = 'edit'
    btn.innerHTML = "remove"
    li.innerHTML = stored[i]
    ul.appendChild(li)
    ul.appendChild(btn)
    ul.appendChild(edit)
    btn.onclick = function() {
      const todos = JSON.parse(localStorage.getItem('todoItem'))
      const indexToDel = todos.indexOf(stored[i])
      todos.splice(indexToDel,1)
      localStorage.setItem('todoItem',JSON.stringify(todos))
      getLocalValue()
    }
    edit.onclick = function() {
      const todos = JSON.parse(localStorage.getItem('todoItem'))
      const indexToEdit = todos.indexOf(stored[i])
      let promt1 =  prompt("Edit Your Task", stored[i])
      todos.splice(indexToEdit,1,promt1)
      localStorage.setItem('todoItem',JSON.stringify(todos))
      getLocalValue()
    }
  }
}
getLocalValue()